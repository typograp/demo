var board = {
  w: undefined,
  h: undefined
}
var font
var poster = {
  noiseAmount: 4000,
  noiseColor: [255, 255, 255]
}

function preload() {
  font = loadFont('ttf/Outfit-Black.ttf')
}

function setup() {
  t = 0
  u = 0
  v = 0
  w = 0
  if (windowWidth <= windowHeight * (9 / 16)) {
    board.w = windowWidth
    board.h = board.w * (16 / 9)
  } else {
    board.h = windowHeight
    board.w = board.h * (9 / 16)
  }
  createCanvas(board.w, board.h)
}

function draw() {
  textFont(font)
  textAlign(CENTER, CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  background(255, 0, 0, 0.025)
  rectMode(CENTER)

  var x = 1 * width * noise(t)
  var y = 1 * height * noise(t + 5)
  var r = 255 * noise(t + 10)
  var g = 255 * noise(t + 15)
  var b = 255 * noise(t + 20)

  noStroke()
  fill(r, g, 255)
  ellipse(x, y, board.w * 1)

  t = t + 0.015

  var x = 1 * width * noise(v + 12)
  var y = 1 * height * noise(v + 11)
  var r = 255 * noise(v + 1)
  var g = 255 * noise(v + 5)
  var b = 255 * noise(v + 2)

  noStroke()
  fill(255, g, b)
  ellipse(x, y, board.w * 0.5)

  v = v + 0.0037

  var x = 1 * width * noise(w + 12)
  var y = 1 * height * noise(w + 11)
  var r = 255 * noise(w + 1)
  var g = 255 * noise(w + 5)
  var b = 255 * noise(w + 2)

  fill(r, g, 255)
  textSize(board.w * 0.175)
  text('demo', x, y)
  textSize(board.w * 0.075)
  push()
  translate(board.w * 0.02 * w, board.w * 0.02 * w)
  text('festival', x, y + board.h * 0.0625)
  pop()

  w = w + 0.0047

  var x = 1 * width * noise(u)
  var y = 1 * height * noise(u + 7)
  var r = 255 * noise(u + 10)
  var g = 255 * noise(u + 15)
  var b = 255 * noise(u + 20)

  fill(255, g, b)
  textSize(board.w * 0.35)
  text('demo', x, y)
  textSize(board.w * 0.15)
  push()
  translate(board.w * 0.02 * u, board.w * 0.02 * u)
  text('festival', x, y + board.h * 0.125)
  pop()

  u = u + 0.0025;

  setNoise()
}

function setNoise() {
  for (var i = 0; i < poster.noiseAmount; i++) {
    var x = Math.random() * width
    var y = Math.random() * height
    var r = Math.random() * poster.noiseColor[0]
    var g = Math.random() * poster.noiseColor[1]
    var b = Math.random() * poster.noiseColor[2]
    stroke(r, g, b)
    strokeWeight(1.5)
    noFill()
    point(x, y)
  }
}

function windowResized() {
  if (windowWidth <= windowHeight * (9 / 16)) {
    board.w = windowWidth
    board.h = board.w * (16 / 9)
  } else {
    board.h = windowHeight
    board.w = board.h * (9 / 16)
  }
  createCanvas(board.w, board.h)
}
